package main

import "fmt"

func main() {
	n := numInput()
	fmt.Println(task(n))
}

func task(n int) int {
	if n == 0 {
		return 0
	}

	i := 1
	cur := 1
	prev := 0
	var tmp int
	for i < n {
		tmp = cur
		cur = tmp + prev
		prev = tmp
		i++
	}

	return cur
}

func numInput() int {
	fmt.Println("Input num between 1 and 10000")
	var n int
	_, err := fmt.Scanf("%d", &n)
	if err != nil || n < 1 || n > 10000 {
		fmt.Println("Incorrect input")
		panic("Incorrect input")
	}
	return n
}
