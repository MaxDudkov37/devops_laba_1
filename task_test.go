package main

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func testTask(t *testing.T) {
	assert.Equal(t, task(0), 0)
	assert.Equal(t, task(1), 1)
	assert.Equal(t, task(2), 1)
	assert.Equal(t, task(3), 2)
	assert.Equal(t, task(4), 3)
	assert.Equal(t, task(5), 5)
	assert.Equal(t, task(6), 8)
	assert.Equal(t, task(7), 13)
	assert.Equal(t, task(8), 21)
	assert.Equal(t, task(9), 34)
	assert.Equal(t, task(10), 55)
	assert.Equal(t, task(11), 89)
	assert.Equal(t, task(12), 144)
	assert.Equal(t, task(13), 233)
	assert.Equal(t, task(14), 377)
	assert.Equal(t, task(15), 610)
}
